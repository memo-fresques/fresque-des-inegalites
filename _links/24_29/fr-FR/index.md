---
fromCardId: '24'
toCardId: '29'
status: valid
---
Les travailleurs pauvres ne peuvent s’extraire de la pauvreté par leur travail, car ils utilisent la totalité de leur revenu pour leur subsistance et ne peuvent pas épargner et se constituer un patrimoine. De plus un grand nombre de travailleurs pauvres dépendent d’une économie informelle où les risques d'accident du travail et de maladie professionnelle sont élevés. N'ayant ni épargne ni protection sociale, ils ne peuvent pas faire face à une perte de revenu.
