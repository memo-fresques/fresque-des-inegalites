---
backDescription: >-
  Les politiques déployées en cas de crise pour relancer l'économie soutiennent
  les actionnaires des industries lourdes et polluantes (automobile,
  aviation...) et des secteurs employant principalement des hommes (bâtiment,
  travaux publics...) En contrepartie, les plans d'austérité restreignent les
  services publics employant principalement des femmes (enfance, santé, soins,
  éducation) et soutenant les responsabilités familiales dont elles assument la
  majeure partie.


  Par contagion, les économies du Sud, économiquement dépendantes des
  importations du Nord, sont indirectement affectées par la récession. Les
  premiers touchés sont les personnes déjà en situation précaire qui risquent de
  basculer dans la pauvreté. Les crises ont donc un effet démultiplicateur des
  inégalités.
title: Les crises économiques accentuent les inégalités
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_06'
---
Les crises économiques jalonnent l’histoire du système capitaliste et seraient inévitables, comme un moyen de régulation naturel des mouvements irrationnels des marchés. Ces crises ont pour point commun d’entraîner une récession brutale qui touche l’ensemble du système économique, affectant notamment l’emploi et les salaires.


Afin de relancer le secteur économique, les politiques publiques mises en œuvre sont en principe destinées à soutenir l’activité des entreprises, au nom de la préservation de l’emploi. Il peut s’agir de politiques monétaires (assouplissement quantitatif) ou budgétaire (relance budgétaire). Elles visent des secteurs employant principalement des hommes (bâtiment, travaux publics) et soutiennent des industries lourdes et polluantes (automobile).


Ces mesures étant liées à un accroissement de la dette publique, elles s’accompagnent de plans d’austérité visant à réduire le déficit budgétaire. Cela se traduit par des reculs en matière sociale. Les services publics en souffrent, notamment les secteurs de l’enfance et du soin. Ces mesures ont un impact particulier sur les femmes, en raison de leurs responsabilités sexospécifiques pour les soins et le bien-être de la famille, et parce que ces secteurs emploient principalement des femmes. Ainsi elles en sont les principales victimes collatérales, avec un effet démultiplicateur sur les inégalités. 
