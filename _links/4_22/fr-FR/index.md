---
fromCardId: '4'
toCardId: '22'
status: valid
---
Il faudrait au moins 69 millions de nouveaux·elles enseignant·es d’ici 2030 pour atteindre l’objectif de développement durable en matière d’éducation. Pourtant, partout dans le monde, les enseignant·es actuellement en poste sont confronté·es à de bas salaires et à des conditions de travail qui se détériorent, ce qui affecte le statut de la profession. La corrélation entre les bas salaires et la pénurie d’enseignant·es résulte clairement de décennies de compressions des dépenses publiques, engendrées plus directement par les réductions imposées
de la masse salariale du secteur public, une mesure d'austérité couramment imposée notamment dans le cadre d'ajustements structurels.
