---
fromCardId: '16'
toCardId: '17'
status: valid
---
Les ajustements structurels sont demandés à l'occasion de prêts du FMI ou de la banque mondiale. Ces prêts impliquent le paiement d'intérêts sur les titres de dette, appelés charge de la dette.
