---
backDescription: >-
  Le travail informel concerne 62 % des travailleuses et travailleurs dans le
  monde


  Dans les pays à faible revenu 92% des femmes et 88% des hommes occupent un
  emploi informel, dangereux ou précaire.
title: Emploi informel prépondérant dans les pays à faible revenu
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_27'
---
Les travailleurs informels sont ceux qui échappent à toute régulation de l’Etat. 
