---
fromCardId: '41'
toCardId: '21'
status: valid
---
La charge très inégale en matière de responsabilités familiales et de soins qui pèse sur les femmes alimente les inégalités sur le marché du travail en termes de types d'emploi auxquels elles peuvent accéder.
