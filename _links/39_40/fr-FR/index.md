---
fromCardId: '39'
toCardId: '40'
status: valid
---
- Les stéréotypes sur les compétences et les qualités des femmes peuvent dissuader les femmes de se présenter à des postes politiques. En effet, les femmes sont souvent perçues comme étant moins compétentes, moins assertives et moins capables de diriger que les hommes. Ces stéréotypes peuvent conduire à une autocensure des femmes, qui peuvent hésiter à se lancer dans une carrière politique.
- Les stéréotypes sur les rôles traditionnels des hommes et des femmes peuvent conduire à une discrimination à l'égard des femmes, qui sont moins susceptibles d'être recrutées ou promues dans des postes de pouvoir.
- Les stéréotypes sur les politiques publiques font que les femmes sont souvent associées à des politiques sociales, telles que l'éducation et la santé. Ces stéréotypes peuvent conduire à ce que les femmes soient moins représentées dans les domaines de la politique étrangère et de la défense, qui sont souvent considérés comme étant plus masculins.
