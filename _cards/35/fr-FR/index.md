---
backDescription: >-
  Plus de la moitié a moins de 18 ans.


  Il existe aussi des millions de personnes apatrides qui ont été privées de
  nationalité et d’accès aux droits élémentaires comme l’éducation, les soins de
  santé, l’emploi et la liberté de circulation.


  Actuellement 1 personne sur 95 a été forcée de fuir son foyer à cause des
  conflits ou de la persécution.
title: '82,4 millions de personnes étaient déracinées à travers le monde à la fin 2020'
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_35'
---
Les causes en sont la persécution, les conflits, les violences, les violations des droits humains ou des événements troublant gravement l'ordre public. Au cours de la dernière décennie (2010-2019), les événements météorologiques ont causé 23 millions de déplacements supplémentaires par an.
