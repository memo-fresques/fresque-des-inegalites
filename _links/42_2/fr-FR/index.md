---
fromCardId: '42'
toCardId: '2'
status: valid
---
La stratégie douanière des pays du Nord leur permet de capter la valeur ajoutée des produits alimentaires transformés (huile, conserves de poisson, chocolat...) au détriment des pays du Sud, privant les populations concernées de ces revenus.
