---
fromCardId: '16'
toCardId: '6'
status: valid
---
Les ajustements structurels impliquent une libéralisation de l'économie qui devient davantage exposée aux crises, une fois devenue vulnérable aux marchés financiers, exposée à la concurrence internationale, et dépendante du commerce mondialisé lui-même soumis à la spéculation. Ces ajustements impliquent aussi une réduction des amortisseurs sociaux, ce qui rend les populations plus vulnérable et accroît les conséquences humaines des crises économiques.
