---
fromCardId: '23'
toCardId: '18'
status: valid
---
Les pays ayant un système de protection sociale complet permettent en principe à leurs citoyens d'avoir une meilleure espérance de vie (ex. la population norvégienne a une meilleure espérance de vie que la population des États-Unis, 81,8 ans vs 78,1 ans). Néanmoins cela n'empêche pas des écarts d'espérance de vie entre plus pauvres et plus riches à l'intérieur d'un même pays.
