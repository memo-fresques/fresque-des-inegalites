---
fromCardId: '3'
toCardId: '17'
status: valid
---
La baisse de l'impôt sur les patrimoines les plus élevés constitue une perte de recettes pour les États, et contribue au déficit budgétaire, lui-même financé par de la dette.
