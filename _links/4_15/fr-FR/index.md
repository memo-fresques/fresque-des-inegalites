---
fromCardId: '4'
toCardId: '15'
status: valid
---
L'austérité a des effets défavorables sur l'accès aux droits essentiels, dont la santé. Les mesures d'austérité peuvent comprendre une participation aux soins hospitalier, la suspension des remboursements et l'augmentation du ticket modérateur pour les produits pharmaceutiques. Ces mesures entraînent une augmentation des dépenses personnelles des populations pour se soigner. En réponse, la qualité de l'offre de services de santé s'en ressent, ce qui provoque des résultats sanitaires en déclin.
