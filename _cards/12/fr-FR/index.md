---
backDescription: >-
  De 2009 à 2016, les entreprises du CAC 40 ont redistribué deux tiers de leurs
  bénéfices en dividendes aux actionnaires et seulement 5,3 % aux salariés.
title: Les bénéfices des multinationales profitent d'abord aux actionnaires
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_12'
---
De 2009 à 2016 les multinationales du CAC 40 ont redistribué 67,4 % de leurs bénéfices aux actionnaires. Entre 2011 et 2021, les 100 plus grandes entreprises cotées sur la bourse française ont versé à leurs actionnaires en moyenne 71% de ce qu’elles ont gagné chaque année. Certaines ont versé des dividendes même lorsqu'elles ont réalisé des pertes.
