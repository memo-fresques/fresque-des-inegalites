---
fromCardId: '13'
toCardId: '11'
status: valid
---
Les importantes rémunérations consenties par les conseils d'administration des multinationales à leurs plus hauts dirigeants sont la contrepartie de la création de valeur pour l'actionnaire, qui peut prendre la forme de versements de dividendes ou de soutien du cours des actions, par la technique du rachat d'actions par exemple.
