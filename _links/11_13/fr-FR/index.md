---
fromCardId: '11'
toCardId: '13'
status: valid
---
La croissance des dividendes a un impact significatif sur les écarts de rémunération. Par exemple en France, les 10 % les plus riches détiennent 60 % des actions. Cela signifie qu'ils bénéficient de la majeure partie de la croissance des dividendes. Cela se fait au détriment des augmentations de salaire ou des créations d'emplois. En effet, les entreprises doivent arbitrer entre la distribution de leurs bénéfices aux actionnaires sous forme de dividendes ou aux salariés sous forme de primes par exemple. Si les dividendes augmentent, les entreprises ont moins de ressources disponibles pour augmenter les salaires ou créer des emplois.
