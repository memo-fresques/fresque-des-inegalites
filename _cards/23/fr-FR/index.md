---
backDescription: >-
  En outre au moins la moitié de la population mondiale (plus de 3,5 milliards
  de personnes) n’ont pas d’accès aux soins de santé essentiels en 2017.
title: >-
  Moins d’un tiers de la population mondiale est couverte par un système de
  sécurité sociale complet
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_23'
---
En 2017 selon le bureau international du travail seul *29 %* de la population mondiale est couverte par une *gamme complète* de prestations (allocations familiales, pension vieillesse…), 16 % bénéficient d'un système incomplet (au moins une prestation), et 55 % n'ont aucune protection.
