---
backDescription: La part des richesses mondiales détenues par les femmes est de 40 %.
title: La richesse des hommes est 50% supérieure à celle des femmes
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_25'
---
Cela résulte de l'écart de rémunération entre les sexes, qui implique que les femmes ne peuvent accumuler autant de richesses que les hommes. Les chiffre de 40 % est en fait une valeur intermédiaire dans la fourchette d'incertitude donnée par le Crédit Suisse en 2018, qui estime que cette part est comprose entre 35 et 42 %.
