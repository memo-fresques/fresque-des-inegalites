---
backDescription: >-
  De 2001 à 2011, près de 700 millions de personnes sortent de la pauvreté
  (seuil de 2 $/personne/jour), mais la plupart reste dans une gamme de faibles
  revenus (moins de 10 $/personne/jour).
title: >-
  Dans le monde, 71 % de la population vit avec moins de 10 $ par jour en 2011
  (en parité de pouvoir d'achat)
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_02'
---
De 2001 à 2011, près de 700 millions de personnes sortent de la pauvreté (seuil de 2$/personne/jour), mais la plupart reste dans une gamme de faibles revenus (moins de 10 $/personne/jour). 
