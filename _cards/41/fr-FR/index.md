---
backDescription: >-
  Partout dans le monde, les femmes et les filles réalisent plus des trois
  quarts de l’ensemble du travail non rémunéré de soin à autrui.
title: Travail non rémunéré de soins à autrui
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_41'
---
Selon l'organisation internationale du travail, partout dans le monde, sans aucune exception, les femmes réalisent la majorité, soit 76,2 %, du travail de soin à autrui non rémunéré, tandis que la contribution moyenne des hommes est inférieure au quart du total. En moyenne, les femmes y consacrent 3,2 fois plus de temps que les hommes : 4 heures et 25 minutes par jour contre 1 heure et 23 minutes. Toujours en moyenne, sur une année, cela représente 201 journées de 8 heures de travail pour les femmes, contre 63 pour les hommes. 

Le temps de travail rémunéré, qu'il soit dans le domaine du soin à autrui ou dans tout autre domaine, est le reflet du temps de travail de soin à autrui non rémunéré. Il est réalisé aux tiers par les femmes et aux deux tiers par les hommes. Au total, le temps de travail des femmes dans le monde, qu'il soit rémunéré ou non, est supérieur de 44 min en moyenne à celui des hommes.
