---
title: "À Propos de la Fresque des Inégalités"
---

La fresque des Inégalités est un atelier d’intelligence collective de 3 h pour un monde juste et soutenable. Parler des inégalités peut être complexe et clivant. La fresque permet d’aborder le sujet de manière ludique, collaborative, visuelle, créative, simple et rigoureuse. A partir de données factuelles issues de sources reconnues, l’atelier permet de décrypter les mécanismes qui créent et maintiennent les inégalités dans le monde, d’en débattre entre participant·es et de déclencher des changements de comportement. A l’issue d’un atelier, tout le monde aura appris des choses, passé un bon moment, et aura envie de passer à l’action. 

L’atelier a été créé par des bénévoles d’[Oxfam France](https://www.oxfamfrance.org) et peut être librement diffusé à tous publics. 