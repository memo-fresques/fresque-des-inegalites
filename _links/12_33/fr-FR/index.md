---
fromCardId: '12'
toCardId: '33'
status: valid
---
En privilégiant la distribution de dividendes aux actionnaires, les multinationales se privent de ressources pour financer les besoins en capitaux nécessaires pour investir dans leur appareil de production, notamment pour assurer la transition vers une production décarbonée.
