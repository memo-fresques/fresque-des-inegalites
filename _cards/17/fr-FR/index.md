---
backDescription: >-
  Plus les pays en développement doivent payer d’intérêts sur leur dette, moins
  ils peuvent dépenser en matière de santé, d'éducation et d’autres services
  essentiels.
title: Service de la dette
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_17'
---
En 2020, six pays (dont le Ghana, la Zambie et la Sierra Léone) dépensaient déjà plus pour le service de la dette (le paiement des intérêts aux créanciers) qu’ils ne dépensent dans les services de santé et d’éducation combinés. Les intérêts de la dette dépendent de la politique monétaire. En période d'inflation, ils sont susceptibles d'être encore plus élevés du fait du relèvement des taux.
