---
backDescription: >-
  Les pays en développement restent à l’écart des vaccins et traitements, ce qui
  est dramatique et a pour conséquence que même avant le covid-19, 90 % des
  décès par maladie infectieuse dans le monde étaient localisés dans les pays en
  développement. La situation est aggravée par la crise du covid-19.
title: Brevets sur les produits de santé
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_19'
---
Les brevets sur les vaccins et traitements sont un obstacle majeur à l'accès de ces produits aux pays en développement. Ils conduisent à des prix élevés et limitent la production locale. La levée des brevets permettrait d'améliorer l'accès aux vaccins et traitements dans les pays en développement.
