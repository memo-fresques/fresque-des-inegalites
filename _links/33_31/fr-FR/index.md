---
fromCardId: '33'
toCardId: '31'
status: valid
---
Pour réduire l'intensité du changement climatique, il est nécessaire d'investir pour décarboner l'appareil productif. Les entreprises qui ne provisionnement pas leurs bénéfices à cette fin, mais au lieu de cela les distribuent aux actionnaires sous forme de dividendes, retardent cette transition et contribuent de fait à aggraver le changement climatique.
