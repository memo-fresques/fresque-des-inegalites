---
fromCardId: '35'
toCardId: '27'
status: valid
---
Les migrants occupent une part significative des emplois informels, en raison des discriminations dont ils font l'objet qui sont une barrière à une activité dans le secteur formel. Ils contribuent ainsi à occuper des emplois dont personne d'autre ne veut, au profit du fonctionnement de l'économie locale. 
