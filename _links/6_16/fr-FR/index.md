---
fromCardId: '6'
toCardId: '16'
status: valid
---
Les crises ont en général pour conséquence de renforcer le déficit budgétaire des États qu'ils financent par de la dette levée sur les marchés financés. Les pays n’ayant pas d’accès au marché financier, qui sont les moins développés, ont recours aux institutions tels que le FMI, qui en contrepartie d’une aide financière impose des ajustements structurels.
