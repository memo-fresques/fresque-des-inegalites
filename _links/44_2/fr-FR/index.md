---
fromCardId: '44'
toCardId: '2'
status: valid
---
Dans le cas du lait, les producteurs européens, qui font face à un prix du lait mondialisé trop bas, ne profitent pas de la politique exportatrice de l'UE, qui se fait au détriment de la valorisation des espaces pastoraux au Sud. Une politique perdant-perdant qui pèse sur les revenus des populations concernées, mais qui profite aux industriels européens de la poudre de lait.
