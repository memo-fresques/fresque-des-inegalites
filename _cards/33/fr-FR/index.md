---
backDescription: >-
  En 2018 encadrer la part versée aux actionnaires à 30 % des bénéfices aurait
  permis de couvrir 98 % des besoins du CAC 40 pour la transition écologique.
title: Pas d'argent pour la transition écologique ?
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_33'
---
De plus, en 2019, 45 % des dividendes et rachats d’actions versés aux actionnaires par les 100 plus grandes entreprises cotées en bourse auraient suffi à couvrir leurs besoins en investissement dans la transformation écologique.
