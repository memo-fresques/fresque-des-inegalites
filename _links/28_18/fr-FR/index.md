---
fromCardId: '28'
toCardId: '18'
status: valid
---
Les inégalités d'espérance de vie sont liées à une surexposition au risque de maladies et d'accidents des catégories de population les plus marginalisées.
