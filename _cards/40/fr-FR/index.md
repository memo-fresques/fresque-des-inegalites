---
backDescription: >-
  De même, les femmes n'occupent que 34% des postes de direction dans les pays
  où les données sont disponibles et moins de 7% dans les quatre pays les moins
  performants (Égypte, Arabie saoudite, Yémen et Pakistan).
title: >-
  En 2018 dans le monde, 18 % des ministres et 24 % des parlementaires sont des
  femmes
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_40'
---
Conséquence : les femmes sont exclues du processus décisionnel

Ce n’est pas une fatalité : la parité totale sur cet indicateur est une réalité dans cinq pays (Bahamas, Colombie, Jamaïque, Laos et Philippines); et dans 19 autres pays, au moins 40% de femmes occupent des postes de direction.

Même lorsqu’elles ont un rôle de représentation politique, les femmes sont reléguées à des postes en fonction des stéréotypes de genre. Par exemple, en France, dans les exécutifs régionaux, seules 11% des délégations des vice-présidences aux Finances et au Budget sont dirigées par des femmes, pour 100% des délégations aux Affaires Sociales et à la Santé. 
