---
backDescription: >-
  N’ayant ni capacité d’épargne, ni accès au crédit, les travailleurs à faible
  revenu du secteur informel ne peuvent pas améliorer leur productivité. Dans la
  mesure où il prive le travailleur de sécurité sociale, notamment de congés
  maladie, le travail informel rend aussi les personnes concernées très
  fragiles.
title: Le travail informel est un piège à pauvreté
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_29'
---
Lorsque la totalité du revenu est consacré à la subsistance, tout accident de vie implique une difficulté à retrouver du travail ne serait-ce que par la difficulté à accéder à une ration qui permet d'être productif. 
