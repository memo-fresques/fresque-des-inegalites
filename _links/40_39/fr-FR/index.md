---
fromCardId: '40'
toCardId: '39'
status: valid
---
La faible proportion de femmes dans les exécutifs politiques renforce l'idée que les postes de pouvoir sont réservés aux hommes. Cela renforce les stéréotypes sur les compétences et les qualités des femmes. Elle contribue à la perception que les femmes ne seraient pas intéressées par la politique. Elle peut aussi conduire à une faible prise en considération au sein des exécutifs de la question de l'égalité femme homme, et notamment la nécessité de lutter contre les stéréotypes de genre qui en sont à l'origine.
