---
fromCardId: '5'
toCardId: '1'
status: valid
---
Les plus riches [en patrimoine] captant davantage de revenus que les plus pauvres, il est logique qu'ils concentrent une part plus importante des richesses.
