---
backDescription: >-
  Les marchés du Nord sont ouverts aux produits tropicaux non transformés qui
  bénéficient de faibles tarifs. En revanche, les produits de base et les
  produits transformés sont beaucoup beaucoup moins avantagés. La progressivité
  des tarifs douaniers en fonction de niveau de transformation des produits
  cantonne les pays en développement à l’exportation de produits primaires,
  permettant aux pays développés de capter l’essentiel de la valeur ajoutée de
  la production alimentaire, et empêchant les pays du Sud de développer leur
  propre industrie.
title: Les tarifs douaniers limitent l’accès des pays du Sud aux marchés du Nord.
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_42'
---
Par exemple, toute entreprise qui veut importer du cacao transformé en Europe paie ces taxes sur le montant d'achat (données de juin 2018) :
- Fèves de cacao séchées : pas de taxe
- Masse ou beurre de cacao : 7,7 %
- Poudre de cacao : 8 %
- Chocolat : 38 à 43 %
