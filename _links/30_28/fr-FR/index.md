---
fromCardId: '30'
toCardId: '28'
status: valid
---
La sous-nutrition peut causer la mort dans les cas les plus sévères si elle n’est pas prise en charge. En outre, chez les enfants malnutris, les dégâts restent durables. Un enfant atteint de malnutrition avant ses 5 ans aura des retards de croissance, des maladies ou des lésions cérébrales entraînant souvent des retards d’apprentissage, des mauvaises performances scolaires et donc plus tard de plus faibles revenus, les laissant dans le cercle vicieux de la faim. D’après l’OMS, dans le monde, 45% des décès d’enfants de moins de 5 ans sont liés à la malnutrition et aux maladies infantiles que ces enfants sous-alimentés sont trop faibles pour combattre.
