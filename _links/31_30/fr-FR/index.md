---
fromCardId: '31'
toCardId: '30'
status: valid
---
L’évolution du régime des précipitations, la hausse des températures et l’augmentation des phénomènes météorologiques extrêmes contribuent à aggraver l’insécurité alimentaire, particulièrement en Afrique, selon les conclusions de l’Organisation météorologique mondiale (OMM).

