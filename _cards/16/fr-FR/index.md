---
backDescription: |-
  Ils pèsent sur les économies les plus en difficulté :
  - réformes des retraites et de la sécurité sociale
  - réduire ou plafonner la masse salariale du secteur public
  - réformes du marché du travail (flexibilité des salaires, licenciements…)
  - réduction des subventions sur les produits de première nécessité
  - réduction de l'assistance sociale et des filets de sécurité
  - introduction ou élargissement des taxes à la consommation (par ex. TVA)
  - privatisation des actifs et des services de l’État
  - réformes des soins de santé
title: >-
  Ajustements structurels en contrepartie de prêts du FMI ou de la banque
  mondiale
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_16'
---
Les ajustements structurels sont des programmes de réformes économiques imposés par le Fonds monétaire international (FMI) et la Banque mondiale aux pays en développement en difficulté économique en contrepartie de prêts. Ils visent à réduire les déficits budgétaires et l'inflation, et à favoriser la croissance économique. Ces programmes sont souvent critiqués pour leurs effets négatifs sur les populations les plus pauvres. Ils entraînent une réduction des services publics, des privatisations d'entreprises publiques, une dérèglementation du marché du travail et une augmentation des taxes. Ils peuvent également entraîner une augmentation des inégalités, car les riches sont plus susceptibles de bénéficier de la croissance économique créée par les ajustements alors que les vulnérables vont en subir les effets négatifs.
