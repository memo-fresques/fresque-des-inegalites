---
fromCardId: '22'
toCardId: '39'
status: valid
---
Les stéréotypes de genre sont de nature culturelle. Ils peuvent être combattus par l'action éducative. Cela suppose que les enfants aient accès au système scolaire et que ce dernier mette en place des actions résolues de lutte contre les stéréotypes.
