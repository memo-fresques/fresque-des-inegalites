---
backDescription: >-
  L’évasion fiscale prive tous les pays de recettes fiscales. Par exemple, pour
  les pays à revenu inférieur, cela représente l’équivalent de 52 % de leur
  budget santé.
title: Evision fiscale et paradis fiscaux
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_14'
---
L'évasion fiscale résulte d'une part du transfert de bénéfices dans les paradis fiscaux par les multinationales pour payer moins d'impôts qu'elles ne devraient, et d'autre part de la dissimulation par les personnes les plus fortunées de revenus ou de capitaux pour échapper à l'impôt. Ces abus ont une incidence beaucoup plus élevée dans les pays à faible revenu. Ces derniers perdent en 2020 l'équivalent de 52% de leur budget santé, contre 8,4 % dans les pays à revenu élevé.
