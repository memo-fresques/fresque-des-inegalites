---
backDescription: >-
  La majorité des exploitations agricoles dans le monde sont familiales et
  paysannes. Elles n’ont aucun pouvoir de négociation face aux fournisseurs
  d’intrants, au négociants, à l’industrie alimentaire et aux supermarchés (10
  chaînes vendaient la moitié des aliments dans l’UE en 2017).
title: >-
  La forte concentration des chaînes d’approvisionnement alimentaire bénéficie
  aux multinationales au détriment des revenus des producteurs
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_43'
---
La grande majorité des exploitations agricoles dans le monde sont familiales et paysannes. 99 % font moins de 50 ha.
Elles n’ont aucun pouvoir de négociation face :
- aux fournisseurs d’intrants : trois conglomérats contrôlent 60 % du marché
- aux négociants : quatre entreprises contrôlent 70 % du négoce
- à l’industrie alimentaire (50 producteurs représentent la moitié de la transformation dans le monde)
- aux supermarchés (10 chaînes vendent la moitié des aliments dans l'UE en 2017).
