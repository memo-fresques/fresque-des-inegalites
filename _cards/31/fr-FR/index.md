---
backDescription: >-
  Ils contribuent à augmenter la pauvreté, l’insécurité alimentaire, les
  problèmes de santé, à rendre plus difficile l’accès à l’eau potable, à
  endommager les infrastructures publiques, à augmenter les inégalités, à créer
  des déplacements, à endommager les écosystèmes, et à déclencher des conflits.
title: Épisodes climatiques extrêmes
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_31'
---
Les épisodes climatiques extrêmes comprennent les canicules, les vagues de froid, les feux de forêt, la sécheresse, les inondations, les submersions marines, les tempêtes tropicales et cyclones, l’érosion côtière. Ils sont augmentés par les émissions de CO2.
