---
backDescription: >-
  Le lobby financier utilise des moyens démesurés pour faire échouer les
  tentatives de régulation des marchés.


  A titre d’exemple, pour influencer la loi Dodd-Franck de 2010 pour réguler les
  marchés financiers aux USA, les financiers de Wall Street disposaient de 20
  fois plus de lobbyistes et de 45 fois plus de moyens financiers que les
  organisations de la société civile, ce qui a conduit à fortement atténuer les
  effets régulateurs de la loi.
title: Lobby financier
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_08'
---
Les groupes financiers ont beaucoup plus de moyens pour réaliser des opérations de lobbiyng contre la régulation financière que les organisations de la société civile pour faire du plaidoyer en sa faveur. Ils peuvent donc obtenir beaucoup plus de rendez-vous avec les décideurs politiques et ainsi atténuer les dispositions qui leur semblent défavorables, puis continuer après l'adoption des règles à faire pression pour qu'elles soient abrogées. Dans l'exemple de la loi Dodd-Franck aux USA visant à réformer le marché financier et à protéger les consommateurs en 2010, les banques disposaient d'un budget 45 fois plus important pour faire du lobbying que les groupes de protection des consommateurs et de 20 fois plus de lobbyistes.
