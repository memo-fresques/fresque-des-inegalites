---
backDescription: >-
  La part de la richesse mondiale détenue par les 1% les plus riches en 2022 est
  de 45,6 % de la richesse mondiale, tandis que la moitié la plus pauvre du
  monde n’en possède que 0,75 %. Pour faire partie des 1% il fallait pour la
  première fois en 2020 détenir plus de 1 millions de $.


  81 milliardaires détiennent en 2022 plus de richesses que 50 % de l’humanité.
  En 2015, Oxfam estimait qu’un tiers des milliardaires devaient leur fortune à
  l’héritage.
title: >-
  Les 1% les plus riches se partagent près de la moitié des richesses des
  ménages dans le monde
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_01'
---
Selon le Crédit Suisse, les 50 % les plus pauvre de la population mondiale détiennent 1% de la richesse mondiale. Les 1% les plus riches détiennent près de 50 % de la richesse mondiale. Pour faire partie des 1 % des plus riches il fallait pour la première fois en 2020 détenir plus de 1 million de $.
