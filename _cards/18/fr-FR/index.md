---
backDescription: Les inégalités sanitaires influencent très fortement l’espérance de vie.
title: Inégalité d'espérance de vie
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_18'
---
Les inégalités sociales ont un impact négatif sur la santé. Les personnes les plus pauvres ont une espérance de vie plus courte que les personnes les plus riches. Ces écarts sanitaires sont dus à un ensemble de facteurs, notamment l'accès aux soins de santé, les conditions de vie et les facteurs socio-économiques.
