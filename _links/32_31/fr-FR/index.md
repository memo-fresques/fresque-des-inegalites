---
fromCardId: '32'
toCardId: '31'
status: valid
---
Les émissions de gaz à effet de serre territoriales restent aujourd’hui principalement liées au niveau de richesse et de développement des pays : un habitant des États-Unis atteignent émet entre 10 et 20 fois plus de CO2-eq qu'un habitant du Sénégal ou du Burkina Fasso. Si les émissions dues à la production de biens sont réattribuées aux pays où les biens sont consommés, l’écart entre pays développés et en développement se creuse davantage par rapport à l’écart en émissions. Les pays industrialisés sont également responsables de 55 % des émissions historiques depuis 1850. A l'intérieur des pays aussi, le principal déterminant des émissions d'un individu est son niveau de richesse. D'un point de vue global, la responsabilité du changement climatique incombe au mode de vie des plus riches, et non à l'ensemble de la population.
