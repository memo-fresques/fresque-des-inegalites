---
fromCardId: '16'
toCardId: '45'
status: valid
---
Parmi les mesures d’ajustement structurel exigées des bailleurs internationaux, certaines ont une incidence défavorable sur la politique agricole. En effet, pour réduire le déficit de la balance des paiements, la priorité est donnée aux cultures d’exportation et aux productions substituables aux produits importés, le tout au détriment de l’autonomie alimentaire. Les mesures d’ajustement monétaires se traduisent souvent par une dévaluation de la devise locale ce qui a pour conséquence de renchérir sur le marché intérieur le prix des productions d’export ou de substitution. La réduction du déficit budgétaire exigé par les bailleurs se fait le plus souvent au détriment des politiques de soutien aux filières agricoles. La recherche d’une productivité accrue impose l’utilisation d’intrants qui, mal maîtrisés, rendent les producteurs dépendants des multinationales qui fournissent ces produits, et créent des risques pour l’environnement, le climat et la biodiversité.
