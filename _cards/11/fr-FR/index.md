---
backDescription: >-
  Les dividendes des entreprises du CAC 40 croissent 3,5 fois plus vite que les
  salaires des personnes qu’elles emploient.
title: Croissance des dividendes des multinationales
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_11'
---
Entre 2009 et 2018, les entreprises françaises du CAC 40 ont vu les dividendes versés à leurs actionnaires augmenter de 70 %. 
