---
fromCardId: '8'
toCardId: '3'
status: valid
---
Le lobby financier a intérêt à faire pression pour diminuer les impôts sur les produits financiers, par exemple les intérêts et dividendes, pour rendre ces investissements plus attractifs. Il s'agit d'impôts payés principalement par les plus riches. Par exemple, en France, le prélèvement forfaitaire unique (dit "flat tax") a été créé par la loi de finances pour 2018 pour s'appliquer aux revenus de l'épargne et du capital. Pour les personnes ayant des revenus importants, il est plus avantageux que l'impôt sur le revenu des personnes physiques.
