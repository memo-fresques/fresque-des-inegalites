---
fromCardId: '6'
toCardId: '35'
status: valid
---
Les crises peuvent avoir ou non un impact sur les flux nets migratoires. Ce qui est certain, c'est que les immigrés d'un pays en crise sont les premiers en difficulté, car ce sont eux qui occupent les emplois précaires, peu qualifiés, ou à temps partiel et qu'on outre ils ont du mal à retrouver du travail en raison de la discrimination à l'embauche. Par ailleurs, les pays en crise peuvent avoir tendance à vouloir réduire les voies d'immigration légale, ce qui a des effets sur l'immigration clandestine et retarde l'intégration des migrants.
