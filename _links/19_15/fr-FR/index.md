---
fromCardId: '19'
toCardId: '15'
status: valid
---
Les brevets sur les produits de santé sont un frein majeur dans l'accès à ces produits pour les populations les plus pauvres, en raison du coût élevé de ces brevets qui peut les rendre inaccessible. Ils incitent à la production de contrefaçons qui peuvent se révéler dangereuses pour les malades.
