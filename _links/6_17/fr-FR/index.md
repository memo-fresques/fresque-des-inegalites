---
fromCardId: '6'
toCardId: '17'
status: valid
---
Les crises économiques peuvent priver les États de recettes fiscales en raison de la contraction de l'activité, et nécessiter un supplément de dépenses pour soutenir les entreprises, et ainsi impliquer un endettement parfois important. Pour les pays déjà endettés, une politique monétaire déflationniste, avec des taux d'intérêts élevés, entraîne une aggravation du service de la dette, le montant des intérêts à verser étant proportionnel aux taux.
