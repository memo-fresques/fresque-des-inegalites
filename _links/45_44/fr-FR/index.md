---
fromCardId: '45'
toCardId: '44'
status: valid
---
La politique agricole d'une zone géographique peut créer des distorsions de marché au détriment d'une autre, en réduisant les coûts de production :
- soit par des aides directes à la production (subventions),
- soit en agissant sur la structuration de la filière, qui gagne en productivité et réduit ainsi ses coûts de production unitaire.
