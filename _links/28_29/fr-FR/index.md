---
fromCardId: '28'
toCardId: '29'
status: valid
---
Les accidents et maladies, lorsqu'ils sont accompagnés d'une incapacité de travailler, privent les travailleurs de revenus. Par ailleurs lorsqu'ils nécessitent des soins, le coût de ceux-ci impliquent des dépenses qui peuvent être importantes. Pour les financer, en absence de revenu, le travailleur peut être amené à céder tout ou partie de son patrimoine, ce qui rend sa situation encore plus précaire. 
