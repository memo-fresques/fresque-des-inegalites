---
fromCardId: '21'
toCardId: '25'
status: valid
---
En moyenne, les femmes passent plus de temps sur le travail de soin non rémunéré que les hommes dans une journée, et les hommes passent plus de temps que les femmes à une occupation lucrative. Ceci s'ajoute au fait qu'à travail égal, les femmes sont en général moins bien payés que les hommes. La conséquence est que les femmes ont un revenu moindre, et logiquement, détiennent un patrimoine inférieur à celui des hommes.
