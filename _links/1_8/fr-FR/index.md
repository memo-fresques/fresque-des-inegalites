---
fromCardId: '1'
toCardId: '8'
status: valid
---
Les personnes les plus riches [en patrimoine] sont notamment actionnaires de groupes bancaires et industriels qui eux-mêmes s'organisent pour défendre leurs intérêts grâce au lobbying.
