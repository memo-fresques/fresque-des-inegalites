---
backDescription: >-
  les activités de soin non rémunérées constituent la principale barrière à
  l’activité économique des femmes et l’une des premières causes du fait
  qu’elles ont de moins bons emplois que les hommes, surtout lorsqu’elles
  assument des responsabilités familiales.
title: >-
  Le travail domestique non rémunéré est une barrière à l'activité économique
  des femmes
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_21'
---
La notion de travail de soin à autrui non rémunéré fait référence à la fourniture de soins au sein du ménage, ou au travail bénévole accompli pour des ménages autres que la famille du bénévole. Il comprend la gestion du ménage (tenue des comptes par ex.), les courses, la préparation des repas, le ménage, l'évacuation des déchets ou leur recyclage, le nettoyage et le lavage des vêtements, la décoration du foyer, le jardinage, éducation des enfants, le soin aux personnes âgées, aux animaux domestiques, le transport et l'accompagnement des personnes, la garde d'enfants, etc. L'arbitrage entre temps passé au travail de soin à autrui non rémunéré et activité professionnelle est un facteur important de l'inégalité femme homme dans le monde du travail. Il conduit à un plus faible taux d'activité pour les femmes, et pour celles qui travaillent, un recours plus fréquent que les hommes au temps partiel, à l'emploi non-salarié, à l'emploi en tant que collaboratrice de l'entreprise familiale ou dans le secteur informel.
