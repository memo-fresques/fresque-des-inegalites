---
fromCardId: '37'
toCardId: '36'
status: valid
---
Lorsque les candidats font appel à des donateurs privés, ils sont plus sensibles aux intérêts de leurs donateurs qu'à ceux de l'électorat. En outre les candidats qui ne disposent pas d'un important soutien financier peuvent être désavantagés par rapport aux candidats qui en ont. Enfin, les dons aux campagnes électorales sont anonymes, ce qui peut rendre difficile de savoir qui influence les décisions des candidats. Cela peut contribuer à un sentiment de défiance des citoyens envers les institutions démocratiques.
