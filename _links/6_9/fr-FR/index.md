---
fromCardId: '6'
toCardId: '9'
status: valid
---
Les politiques de relance en réponse aux crises économiques prévoient souvent des mesures de réduction de la fiscalité des entreprises.
