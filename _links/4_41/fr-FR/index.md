---
fromCardId: '4'
toCardId: '41'
status: valid
---
Les coupes budgétaires résultant des politiques d'austérité affectent les services publics de la petite enfance, de la santé, de la prise en charge des personnes âgées, de l'éducation, des centres de loisirs, ce qui met à la charge des familles davantage de travail de soins non rémunéré.
