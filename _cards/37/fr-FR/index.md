---
backDescription: >-
  Depuis plusieurs années, le financement public de la démocratie est mis à mal
  en Europe comme en Amérique du Nord. Ce financement public insuffisant profite
  aux candidats qui sont financés par les plus riches donateurs. Ces derniers
  bénéficient d'une réduction fiscale... payée par la collectivité, en majorité
  plus pauvre. C’est le cas aussi en France : les dons aux partis politiques
  peuvent aller jusqu’à 15 000 € par foyer fiscal et sont défiscalisés à hauteur
  de 66 %.
title: >-
  Les plus riches se sont accaparés le financement de la démocratie et les
  pauvres paient
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_37'
---
Remarque : le montant de 15000 € suppose un foyer fiscal avec 2 personnes. Le don est limité à 7500 € par personne. Selon Julia Cagé, Les donateurs parmi les 0,01% des Français aux revenus les plus élevés ont donné en moyenne 5 245 euros par an, non loin du maximum autorisé. Les donateurs parmi les 0,01% des Français aux revenus les plus élevés touchent en moyenne 3 900 euros d’argent public par an, remboursés sous forme de déduction fiscale, contre 73 euros pour les 40% les plus modestes, soit 53 fois plus. Ce système a une conséquence : ce sont les plus riches qui captent la plus grande partie du financement public indirect des mouvements politiques en France.

Les grands partis de droite reçoivent  beaucoup plus de dons privés que leurs équivalents de gauche. Cet inégal accès aux dons privés n’est que très partiellement compensé par les autres formes de financement, qu’il s’agisse des contributions des élus ou des cotisations des adhérents. Si à droite, ce sont les riches qui donnent, à gauche, ce sont les adhérents et les élus qui contribuent à la bonne santé financière de leur parti.
