---
fromCardId: '12'
toCardId: '5'
status: valid
---
Le fait que les multinationales privilégient le versement de dividendes profite aux détenteurs de capitaux, qui sont la fraction de la population qui est déjà la plus riche.
