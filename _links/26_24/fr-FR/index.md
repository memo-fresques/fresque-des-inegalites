---
fromCardId: '26'
toCardId: '24'
status: valid
---
La faible rémunération dans le secteur informel et sa prédominance dans les économies les moins développées explique que dans ces pays une forte proportion de travailleurs sont très pauvres. Globalement, dans les pays en développement, la plupart des pauvres travaillent.
