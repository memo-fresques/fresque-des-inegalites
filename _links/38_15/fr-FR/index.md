---
fromCardId: '38'
toCardId: '15'
status: valid
---
L'aide publique non versée aux pays en développement ne permet pas à ces derniers de financer l'accès aux services essentiels, notamment les services de santé.
