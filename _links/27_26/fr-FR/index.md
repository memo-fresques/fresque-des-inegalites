---
fromCardId: '27'
toCardId: '26'
status: valid
---
L'emploi informel se situe dans les secteurs où l'on retrouve les revenus les plus bas : couture, ferraille, recyclage, emballage et portage de courses, vendeur de rue, barbier coiffeur itinérant, ouvrier journalier dans la construction, travaux agricole, élevage... Ces emplois étant eux-mêmes situés dans les pays à faible revenu, ils sont au plus bas de l'échelle des revenus dans le monde.
