---
backDescription: >-
  Au niveau mondial, au cours des dix dernières années, les milliardaires ont
  multiplié leur fortune par deux, soit près de six fois plus que l’augmentation
  des richesses des 50 % les plus pauvres.


  Pour les individus situés entre ces deux catégories (et notamment pour
  l’ensemble des classes moyennes et populaires nord-américaines et
  européennes), la croissance du revenu a été faible.
title: >-
  La croissance du revenu mondial profite six fois plus aux 1% les plus riches
  qu’aux 50% les plus pauvres
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_05'
---
Depuis 2020, deux tiers des richesses mondiales produites ont été captées par les 1% les plus riches.

Au niveau mondial, au cours des dix dernières années, les milliardaires ont multiplié leur fortune par deux, soit près de six fois plus que l’augmentation des richesses des 50 % les plus pauvres.

En France, sur les 10 dernières années, en moyenne, pour 100 € de richesses créées, 35 € ont été captés par les 1 % des Français·es les plus riches, 32 € par les 9 % suivants. Les 50 % les plus précaires n’en ont capté que 8 €. La croissance du revenu national profite donc 4 fois plus aux 1 % les plus riches qu’aux 50 % les plus pauvres.
