---
fromCardId: '36'
toCardId: '38'
status: optional
---
Les bailleurs de fonds sont souvent réticents à fournir de l'aide aux pays qui ne respectent pas les principes démocratiques. Lorsque l'aide est fournie à des pays qui ne sont pas démocratiques, elle est plus susceptible d'être gaspillée ou détournée. Il est également plus difficile de s'assurer l'aide est utilisée de manière transparente et responsable. 

Néanmoins, les promesses non tenues des pays donateurs sont surtout dues au fait que l'APD n'est pas une priorité budgétaire.

