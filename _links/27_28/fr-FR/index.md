---
fromCardId: '27'
toCardId: '28'
status: valid
---
Dans le travail informel, la prévention des accidents du travail et des maladies professionnelles n'est pas assurée, ce qui expose le travailleur à un risque accru d'accident et de maladie.
