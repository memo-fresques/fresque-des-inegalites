---
fromCardId: '43'
toCardId: '2'
status: valid
---
Environ un quart du total des emplois du monde se trouve dans le secteur agricole, et beaucoup plus dans les pays à faible revenu (70 % au Mali, Burkina Faso, Niger, Tchad, RCA...) Une proportion importante de la population mondiale subit donc la pression des chaînes d'approvisionnement sur leur revenu.
