---
backDescription: >-
  Les nations riches n’ont consacré que 0,3 % de leur revenu à l’aide
  internationale en 2019 alors qu’ils se sont engagés sur 0,7 % depuis 1970.
title: Aide publique au développement
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_38'
---
24/10/1970 : il y a 50 ans, l'Assemblée générale de l'ONU acte l'engagement des pays riches de dédier 0,7% de leur richesse nationale à l'aide au développement. 

Dès 1974 la Suède atteint les 0,7%, les Pays-Bas en 1975, le Royaume-Uni les atteint en 2013 et inscrit cette aide internationale dans la loi.

L’aide internationale a contribué à éradiquer presque totalement la poliomyélite du monde et évité à 18 millions de personnes la paralysie. Elle a connu également des succès dans le domaine de la santé pour lutter contre le sida, la tuberculose et le paludisme, et dans le domaine éducatif pour renforcer les systèmes d’enseignement dans les pays à revenus faibles et intermédiaires. Lorsqu’elle est utilisée de manière appropriée, elle soutient durablement les progrès de la lutte contre les inégalités et la pauvreté.

Néanmoins les pays pauvres ont été privés au cours des 50 dernières années de *5 000 milliards d’euros* (soit 100 milliards d’euros par an), car la majorité des pays riches n’ont pas tenu sur leur « promesse solennelle » de consacrer 0,7 % de leur revenu national à l’aide internationale. Dans le cas de la France, qui n’a jamais tenu son engagement des 0,7%, cela représente 200 milliards d’euros sur un demi-siècle.

Ce chiffre représente *9 fois la dette de l’Afrique subsaharienne*. 
