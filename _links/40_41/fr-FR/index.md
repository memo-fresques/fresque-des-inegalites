---
fromCardId: '40'
toCardId: '41'
status: valid
---
La faible représentation des femmes dans les postes de pouvoir peut conduire à des politiques publiques qui ne soutiennent pas suffisamment les femmes et les familles. Par exemple, les politiques qui ne soutiennent pas suffisamment les services de garde d'enfants peuvent rendre plus difficile pour les femmes de concilier leur vie professionnelle et leur vie familiale. Cela peut conduire à ce que les femmes assument une plus grande part des responsabilités de soins non rémunérés.
