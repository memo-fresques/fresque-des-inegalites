---
backDescription: >-
  Les stéréotypes de genre conduisent à la persistance des inégalités femme -
  homme dans la société.
title: Stéréotypes de genre
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_39'
---
Les stéréotypes de genre sont des représentations schématiques et globalisantes sur ce que sont et ne sont pas les filles et les garçons, les femmes et les hommes.

Ce type de stéréotype impacte de nombreux aspects de la vie scolaire, sociale et professionnelle : 
- le rapport au marché du travail
- le rôle dévolu aux femmes dans les soins ou l’éducation
- l’éducation différenciée selon le sexe
- l’idée que les femmes et les hommes ont des compétences innées différentes ce qui accrédite la pensée que les inégalité observées sont prétendument justifiées
