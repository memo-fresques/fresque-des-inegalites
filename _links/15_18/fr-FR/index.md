---
fromCardId: '15'
toCardId: '18'
status: valid
---
Les difficultés d’accès aux soins de santé ont un impact défavorable sur l'espérance de vie.
