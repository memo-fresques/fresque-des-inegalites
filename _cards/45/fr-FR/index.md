---
backDescription: >-
  Impact environnemental : la politique agricole est responsable de 24% des
  émissions de GES et 80% de la déforestation.


  Impact social : 2/3 des travailleurs pauvres dans le monde sont dans le
  secteur agricole.


  En outre, la politique agricole échoue à résoudre les problèmes d’insécurité
  alimentaire dans le monde.
title: 'Politique agricole : impact environnemental et social'
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_45'
---
Le système agroalimentaire actuel est basé sur la domination du modèle industriel, friand en énergies fossiles, indifférent à l'impact environnemental, et concentre les richesses entre les mains d’un petit nombre d’acteurs, laissant pour compte les plus petits producteurs.
