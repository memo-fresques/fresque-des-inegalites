---
backDescription: >-
  Lorsque la dérégulation est forte, les 1% des plus riches sont en mesure
  d’accaparer une forte proportion des revenus nationaux.
title: Dérégulation financière
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_07'
---
Il y a une corrélation directe entre la dérégulation financière et les inégalités.

Par régulation financière on entend notamment la séparation des banques de dépôt et d’investissement (Glass-Steagall act aux US), le plafonnement des intérêts, la séparation des activités de banque et d’assurance et la concentration des établissements financiers.

Historiquement, plus la dérégulation a été forte, plus les 1% des plus riches ont été en mesure d’accaparer une forte proportion des revenus nationaux, et inversement.
