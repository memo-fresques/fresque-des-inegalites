---
fromCardId: '7'
toCardId: '6'
status: valid
---
Le nombre et la gravité des crises économiques augmente conjointement à la généralisation de la dérèglementation financière à l'échelle mondiale.
