---
backDescription: >-
  Plus de 80% de la population mondiale est concernée par des mesures
  d'austérité budgétaire.
title: Austérité budgétaire
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_04'
---
En 2023, 85 % de la population mondiale devrait vivre sous l’emprise de mesures d’austérité budgétaire.

Il s'agit notamment : 
1. de l'élimination ou de la réduction des subventions, y compris sur les carburants, l'agriculture et les produits alimentaires ; 
2. de réduire / plafonner la masse salariale, y compris les salaires des travailleurs de l'éducation, de la santé et des autres travailleurs du secteur public ; 
3. de rationaliser et cibler davantage les prestations de sécurité sociale ; 
4. des réformes des retraites ; 
5. des réformes du marché du travail ; 
6. des réformes du système de santé. 

Il peut également être question d'augmenter la TVA qui touche davantage les personnes consacrant une part importante de leur revenu aux dépenses de subsistance, ou de privatiser les services publics.

Contrairement à la perception du public, les mesures d'austérité ne se limitent pas à l'Europe; en fait, bon nombre des principales mesures d'ajustement sont plus importantes dans les pays en développement. 
