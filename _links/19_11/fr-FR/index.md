---
fromCardId: '19'
toCardId: '11'
status: valid
---
Les brevets sur les produits de santé sont une source importante de revenus pour les entreprises du secteur de la pharmacie, qui contribuent à la distribution de dividendes aux actionnaires de ces entreprises.
