---
fromCardId: '16'
toCardId: '23'
status: valid
---
Un des objectifs des ajustements structurels étant de réduire le coût du travail, ils comportent généralement des mesures qui directement ou indirectement affectent la protection sociale. Lorsque la protection sociale est directement assurée par l'entreprise sous forme de prestation en nature (par exemple lorsque l'employeur fournit un logement à ses employés), l'exposition à la concurrence internationale résultant du programme d'ajustements structurels l'amène à abandonner ce type d'avantage pour rester compétitif.
