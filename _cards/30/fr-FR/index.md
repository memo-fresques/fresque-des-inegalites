---
backDescription: >-
  L’insécurité alimentaire aigüe est celle qui menace les vies et moyens de
  subsistance et nécessite une action humanitaire urgente.


  Causes : conflits et insécurité, crises économiques et sanitaires, épisodes
  climatiques extrêmes.
title: >-
  En 2020 l’insécurité alimentaire aigüe a augmenté et touché 155 millions de
  personnes dans le monde
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_30'
---
*Définition de l’insécurité alimentaire* : privations de nourriture menaçant des vies ou des moyens d'existence ou les deux, quelles qu'en soient les causes, le contexte ou la durée.
