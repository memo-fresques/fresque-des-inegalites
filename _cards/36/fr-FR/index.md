---
backDescription: >-
  En 2021, moins de la moitié de la population mondiale se trouvait dans un pays
  classé démocratie à part entière ou démocratie imparfaite (45,7 %) et
  seulement 6,4 % dans une démocratie à part entière, selon le classement
  réalisé par The Economist.


  Entre 2015 et 2021, l’indice de démocratie dans le monde n’a cesséde reculer.
  En 2021, il a connu sa plus forte chute historique depuis 2010 et son plus bas
  depuis sa création en 2006.
title: Recul démocratique
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_36'
---
L’étude sur l’indice de démocratie de The Economist attribue une note aux nations en fonction de leur respect des principes démocratiques. L'enquête annuelle, qui classe l'état de la démocratie dans 167 pays sur la base de cinq critères – processus électoraux et pluralisme, fonctionnement du gouvernement, participation politique, culture politique démocratique et libertés civiles – révèle que plus d'un tiers de la population mondiale était soumise à des régimes autoritaires en 2022, tandis que seulement 6,4 % jouissaient d’une démocratie totale. Les indications sont combinées pour attribuer une note de 0 à 10 à chaque catégorie, et les scores des cinq catégories sont additionnés pour obtenir le score global de l'indice. Les démocraties complètes sont des pays dont l’indice de démocratie total se situe entre 8,01 et 10 (sur 10). Ceux qui obtiennent un score compris entre 6,01 et 8,00 sont classés dans la catégorie des démocraties imparfaites. Selon la publication, la démocratie est à son plus bas niveau depuis la création de l’indice en 2006, ce qui peut être attribué en partie aux limitations liées à la pandémie qui ont vu de nombreux gouvernements lutter pour combiner santé publique et liberté personnelle. Selon l’évaluation de 2023, la Norvège est le pays le plus démocratique, tandis que l’Afghanistan est le moins démocratique. 
