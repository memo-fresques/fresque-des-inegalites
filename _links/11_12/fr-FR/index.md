---
fromCardId: '11'
toCardId: '12'
status: valid
---
La croissance des dividendes des multinationales a pour effet de modifier le partage de la valeur créée par celles-ci de manière disproportionnée au profit des actionnaires et nuit à l'investissement dans l'appareil productif.
