---
fromCardId: '35'
toCardId: '28'
status: valid
---
Selon l'OMS, Les migrants et les réfugiés peuvent tomber malades en situation de transit ou lorsqu’ils séjournent dans les pays d’accueil en raison des mauvaises conditions de vie ou des changements de leur mode de vie. Les camps avec des abris ou un assainissement de fortune, ou des changements de mode vie, avec pas assez de nourriture et d’eau et un stress accru sont des facteurs de risque supplémentaires. On signale aussi couramment la dépression et l’anxiété, liées à des procédures prolongées de demande d’asile et de mauvaises conditions socioéconomiques, comme le chômage et l’isolement.
