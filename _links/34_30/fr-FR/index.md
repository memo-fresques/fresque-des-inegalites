---
fromCardId: '34'
toCardId: '30'
status: valid
---
Les conflits sont le facteur d’insécurité alimentaire principal. La violence proche limite l'accès à la terre, aux semences, à l'eau et à un travail digne. En raison de l’interdépendance et la fragilité des systèmes alimentaires mondiaux, les conflits éloignés causent également de graves conséquences pour la sécurité alimentaire et nutritionnelle au plan mondial. Les pays se trouvant déjà aux prises avec la faim aiguë à des degrés élevés sont particulièrement vulnérables, à cause notamment de leur forte dépendance à l’égard des importations de denrées alimentaires et d’intrants agricoles et de leur vulnérabilité face à la flambée des prix mondiaux des denrées alimentaires.

