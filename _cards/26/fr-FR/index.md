---
backDescription: >-
  Selon la Banque mondiale les travailleurs informels ont en moyenne des revenus
  19 % inférieurs à ceux des travailleurs formels et ont une épargne limitée.


  Ils sont également moins susceptibles d'être éligibles aux aides publiques
  pour aider les personnes et les entreprises à traverser les difficultés.
title: Faible rémunération dans le secteur informel
wikiUrl: 'http://inegalites.sgrt.eu/w/index.php/Carte_26'
---
Les travailleurs informels sont ceux qui échappent à toute régulation de l’Etat. 
